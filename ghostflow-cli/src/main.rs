// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! ghostflow-cli
//!
//! This is a command line tool which may be used to perform workflow actions on a repository.

use std::num::ParseIntError;
use std::process::Command;
use std::sync::Arc;

use clap::builder::PossibleValuesParser;
use clap::{Arg, ArgAction};
use git_workarea::GitError;
use log::LevelFilter;
use rayon::ThreadPoolBuilder;
use termcolor::ColorChoice;
use thiserror::Error;

mod exit_code;
use exit_code::ExitCode;

#[doc = include_str!("../doc/checks.md")]
pub mod checks;

#[doc = include_str!("../doc/formatters.md")]
pub mod formatters {}

mod config;

mod host;
use host::{Ci, CiError, Local, LocalError, LocalService};

mod command;
use command::check::{Check, CheckError};
use command::reformat::{Reformat, ReformatError};

#[derive(Debug, Error)]
#[non_exhaustive]
enum SetupError {
    #[error("unrecognized logger: `{}`", logger)]
    UnrecognizedLogger { logger: String },
    #[error("non-integer thread count {}: {}", count, source)]
    NonIntegerThreadCount {
        count: String,
        #[source]
        source: ParseIntError,
    },
    #[error("unknown --color value '{}'", color)]
    UnknownColor { color: String },
    #[error("failed to find `.git` directory: {}", output)]
    FindGitDir { output: String },
    #[error("unknown command '{}'", command)]
    UnknownCommand { command: String },
    #[error("failed to initialize the global rayon thread pool: {}", source)]
    RayonThreadPoolInit {
        #[from]
        source: rayon::ThreadPoolBuildError,
    },
    #[error("failed to initialize the local service: {}", source)]
    ServiceInit {
        #[from]
        source: LocalError,
    },
    #[error("failed to initialize CI service: {}", source)]
    Ci {
        #[from]
        source: CiError,
    },
    #[error("git error: {}", source)]
    Git {
        #[from]
        source: GitError,
    },
    #[error("`check` error: {}", source)]
    Check {
        #[from]
        source: CheckError,
    },
    #[error("`reformat` error: {}", source)]
    Reformat {
        #[from]
        source: ReformatError,
    },
}

impl SetupError {
    fn unrecognized_logger(logger: String) -> Self {
        SetupError::UnrecognizedLogger {
            logger,
        }
    }

    fn non_integer_thread_count(count: String, source: ParseIntError) -> Self {
        SetupError::NonIntegerThreadCount {
            count,
            source,
        }
    }

    fn unknown_color(color: String) -> Self {
        SetupError::UnknownColor {
            color,
        }
    }

    fn find_git_dir(output: &[u8]) -> Self {
        SetupError::FindGitDir {
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn unknown_command(command: String) -> Self {
        SetupError::UnknownCommand {
            command,
        }
    }
}

enum Logger {
    Env,
}

fn try_main() -> Result<ExitCode, SetupError> {
    let matches = clap::Command::new("ghostflow-cli")
        .version(clap::crate_version!())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Perform ghostflow actions")
        .arg(
            Arg::new("DEBUG")
                .short('d')
                .long("debug")
                .help("Increase verbosity")
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("LOGGER")
                .short('l')
                .long("logger")
                .default_value("env")
                .value_parser(PossibleValuesParser::new(["env"]))
                .help("Logging backend")
                .value_name("LOGGER")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("THREAD_COUNT")
                .short('j')
                .long("threads")
                .help("Number of threads to use in the Rayon thread pool")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("CI")
                .short('c')
                .long("ci")
                .help("Continuous integration environment")
                .action(ArgAction::Set)
                // CI environments tell us where the repository is.
                .conflicts_with("REPOSITORY")
                // Color only really matters with a TTY.
                .conflicts_with("COLOR"),
        )
        .arg(
            Arg::new("REPOSITORY")
                .short('r')
                .long("repo")
                .help("Path to the repository")
                .default_value(".")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("COLOR")
                .long("colors")
                .help("Use colors when writing output")
                .default_value("auto")
                .value_parser(PossibleValuesParser::new([
                    "always",
                    "always-ansi",
                    "false",
                    "never",
                    "auto",
                    "true",
                ]))
                .action(ArgAction::Set),
        )
        .subcommand(Check::subcommand())
        .subcommand(Reformat::subcommand())
        .get_matches();

    let log_level = match matches.get_count("DEBUG") {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let _logger = match matches
        .get_one::<String>("LOGGER")
        .expect("logger should have a value")
        .as_str()
    {
        "env" => {
            env_logger::Builder::new().filter(None, log_level).init();
            Logger::Env
        },

        logger => {
            return Err(SetupError::unrecognized_logger(logger.into()));
        },
    };

    log::set_max_level(log_level);

    if let Some(count) = matches.get_one::<String>("THREAD_COUNT") {
        let count = count
            .parse::<usize>()
            .map_err(|err| SetupError::non_integer_thread_count(count.into(), err))?;

        ThreadPoolBuilder::new().num_threads(count).build_global()?;
    }

    let service: Arc<dyn LocalService> = if let Some(service) = matches.get_one::<String>("CI") {
        Ci::create(service)?.into()
    } else {
        let repo = matches
            .get_one::<String>("REPOSITORY")
            .expect("--repo has a default");
        let gitdir = {
            let rev_parse = Command::new("git")
                .arg("rev-parse")
                .arg("--absolute-git-dir")
                .current_dir(repo)
                .output()
                .map_err(|err| GitError::subcommand("rev-parse", err))?;
            if !rev_parse.status.success() {
                return Err(SetupError::find_git_dir(&rev_parse.stderr));
            }

            String::from_utf8_lossy(&rev_parse.stdout)
                .trim_end()
                .to_string()
        };
        let color_choice = matches
            .get_one::<String>("COLOR")
            .expect("--color has a default")
            .as_str();
        let color_choice = match color_choice {
            "always" => ColorChoice::Always,
            "always-ansi" => ColorChoice::AlwaysAnsi,
            "false" | "never" => ColorChoice::Never,
            "true" | "auto" => ColorChoice::Auto,
            color => return Err(SetupError::unknown_color(color.into())),
        };

        Arc::new(Local::new(gitdir, color_choice)?)
    };

    let status = match matches.subcommand() {
        Some(("check", m)) => Check::run(service, m)?,
        Some(("reformat", m)) => Reformat::run(service, m)?,
        Some((subcmd, _)) => return Err(SetupError::unknown_command(subcmd.into())),
        None => ExitCode::Success,
    };

    Ok(status)
}

fn main() {
    match try_main() {
        Ok(code) => {
            code.exit();
            unreachable!()
        },
        Err(err) => panic!("{:?}", err),
    }
}
