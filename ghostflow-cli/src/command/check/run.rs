// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use clap::{Arg, ArgAction, ArgMatches, Command};
use thiserror::Error;

use crate::checks::formatter::{Formatter, FormatterError};
use crate::exit_code::ExitCode;
use crate::host::LocalService;

mod commits;
use self::commits::{Commits, CommitsError};

mod topic;
use self::topic::{Topic, TopicError};

pub struct Run;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum RunError {
    #[error("the `check run` subcommand requires a subcommand")]
    NoSubcommand,
    #[error("unknown `check run` subcommand `{}`", subcommand)]
    UnknownCommand { subcommand: String },
    #[error("formatter error: {}", source)]
    Formatter {
        #[from]
        source: FormatterError,
    },
    #[error("{}", source)]
    Commits {
        #[from]
        source: CommitsError,
    },
    #[error("{}", source)]
    Topic {
        #[from]
        source: TopicError,
    },
}

impl RunError {
    fn unknown_command(subcommand: String) -> Self {
        RunError::UnknownCommand {
            subcommand,
        }
    }
}

type RunResult<T> = Result<T, RunError>;

impl Run {
    pub fn run(service: Arc<dyn LocalService>, matches: &ArgMatches) -> RunResult<ExitCode> {
        matches
            .get_many::<String>("FORMATTER")
            .map(|vs| Formatter::parse_args(vs.map(|v| v.as_str())))
            .transpose()?;

        let config = matches.get_one::<String>("CONFIG").map(|s| s.as_ref());

        match matches.subcommand() {
            Some(("commits", m)) => Ok(Commits::run(service, m, config)?),
            Some(("topic", m)) => Ok(Topic::run(service, m, config)?),
            Some((subcmd, _)) => Err(RunError::unknown_command(subcmd.into())),
            None => Err(RunError::NoSubcommand),
        }
    }

    pub fn subcommand() -> Command {
        Command::new("run")
            .about("run checks")
            .arg(
                Arg::new("CONFIG")
                    .short('c')
                    .long("config")
                    .help("Configuration for checks")
                    .action(ArgAction::Set),
            )
            .arg(
                Arg::new("FORMATTER")
                    .short('F')
                    .long("formatter")
                    .help("specify the path to a formatter `KIND=PATH`")
                    .action(ArgAction::Append),
            )
            .subcommand(Commits::subcommand())
            .subcommand(Topic::subcommand())
    }
}
