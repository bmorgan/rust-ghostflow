// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::iter;
use std::sync::Arc;

use git_workarea::CommitId;

use crate::actions::merge::*;
use crate::host::HostingService;
use crate::tests::merge::utils;
use crate::tests::mock::{MockData, MockMergeRequest, MockService};
use crate::tests::utils::test_workspace_dir;

const REPO_NAME: &str = "base";
const MR_ID: u64 = 1;
const MR_UPDATE_ID: u64 = 2;
const MR_CONFLICT_ID: u64 = 6;
const MR_REJECTED_BY_ID: u64 = 7;
const COMMIT: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
const COMMIT_UPDATE: &str = "fe70f127605efb6032cacea0bd336428d67ed5a3";
const COMMIT_CONFLICT: &str = "7a28f8ea8759ff4f125ddbca825f976927a61310";

const BRANCH_NAME: &str = "test-merge";
const BACKPORT_BRANCH_NAME: &str = "test-merge-backport";
const BACKPORT_BRANCH_NAME2: &str = "test-merge-backport2";
const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";
const BASE_UPDATE: &str = "11bfbf44147015650afe6f95508ecfb1a77443cd";

fn merge_service() -> Arc<MockService> {
    let base = MockData::repo(REPO_NAME);
    let fork = MockData::forked_repo("fork", &base);
    let user = MockData::user("user");
    let maint = MockData::user("maint");
    MockData::builder()
        .add_merge_request(MockMergeRequest::new(MR_ID, &user, COMMIT, &base, &fork))
        .add_merge_request(
            MockMergeRequest::new(MR_UPDATE_ID, &user, COMMIT_UPDATE, &base, &fork)
                .old_commit(COMMIT),
        )
        .add_merge_request(MockMergeRequest::new(
            MR_CONFLICT_ID,
            &user,
            COMMIT_CONFLICT,
            &base,
            &fork,
        ))
        .add_merge_request(
            MockMergeRequest::new(MR_REJECTED_BY_ID, &user, COMMIT, &base, &fork)
                .add_comment(MockData::comment(&maint, "Rejected-by: me")),
        )
        .add_project(base)
        .add_project(fork)
        .add_user(maint)
        .add_user(user)
        .service()
}

// Duplicate target branches should fail.
#[test]
fn test_merge_backport_duplicate_branches() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let err = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap_err();

    if let MergeError::DuplicateTargetBranch {
        branch,
    } = err
    {
        assert_eq!(branch, BRANCH_NAME);
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: add a base commit\n",
    );
}

// Trying to merge an unrelated commit should fail.
#[test]
fn test_merge_backport_unrelated_commits() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, Some(CommitId::new(BASE_UPDATE))),
    ];
    let err = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap_err();

    if let MergeError::UnrelatedCommit {
        commit,
    } = err
    {
        assert_eq!(commit.as_str(), BASE_UPDATE);
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: add a base commit\n",
    );
}

// Conflicts should be reported.
#[test]
fn test_merge_backport_conflict() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_CONFLICT_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request contains conflicts with `test-merge` in the following paths:\n\n  - \
         `base`",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: update base commit\n",
    );
}

// Conflicts should be reported in the right order.
#[test]
fn test_merge_backport_conflict_order() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_CONFLICT_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request contains conflicts with `test-merge-backport` in the following \
         paths:\n\n  - `base`",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: update base commit\n",
    );
}

#[test]
fn test_merge_backport_policy_rejection() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_REJECTED_BY_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge` because:\n\n  - rejected by \
         @maint",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: add a base commit\n",
    );
}

#[test]
fn test_merge_backport_policy_rejection_order() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_REJECTED_BY_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge-backport` because:\n\n  - rejected \
         by @maint",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: add a base commit\n",
    );
}

#[test]
fn test_merge_backport_atomic_push() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    // Make a change on remote the clone is not aware of.
    utils::make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "Automatic merge succeeded, but pushing to the remote failed.",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
    utils::check_mr_commit_message(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "base: add a base commit\n",
    );
}

#[test]
fn test_merge_backport() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

#[test]
fn test_merge_backport_as_named() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    merge_settings.elide_branch_name(true);
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1'\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-1' into custom-branch\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

#[test]
fn test_merge_backport_different_commits() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();

    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(
            &merge_settings2,
            Some(mr.old_commit.as_ref().unwrap().id.clone()),
        ),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-2' into test-merge\n\
         \n\
         fe70f12 topic-1: update\n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !2\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-2' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !2\n",
    );
}

#[test]
fn test_merge_backport_different_commits_as_named() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    merge_settings.elide_branch_name(true);
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();

    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(
            &merge_settings2,
            Some(mr.old_commit.as_ref().unwrap().id.clone()),
        ),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-2'\n\
         \n\
         fe70f12 topic-1: update\n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !2\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-2' into custom-branch\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !2\n",
    );
}

#[test]
fn test_merge_backport_many_branches() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    let merge_settings3 =
        MergeSettings::new(BACKPORT_BRANCH_NAME2, utils::TestMergePolicy::default());
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings3, None),
    ];
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME2,
        "Merge topic 'topic-1' into test-merge-backport2\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

#[test]
fn test_merge_backport_many_branches_as_named() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    merge_settings.elide_branch_name(true);
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let mut merge_settings3 =
        MergeSettings::new(BACKPORT_BRANCH_NAME2, utils::TestMergePolicy::default());
    merge_settings3.merge_branch_as(Some("custom-branch2"));
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings3, None),
    ];
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1'\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-1' into custom-branch\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME2,
        "Merge topic 'topic-1' into custom-branch2\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// backport -> branch (mr to both)
#[test]
fn test_merge_backport_into() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings2.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge branch 'test-merge-backport' into test-merge",
    );
    utils::check_noop_merge(&origin_ctx, BRANCH_NAME);
    let parent = format!("{}~", BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        &parent,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// backport2 -> backport -> branch (mr to just backports)
#[test]
fn test_merge_backport_into_backports() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut merge_settings =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME2, utils::TestMergePolicy::default());
    merge_settings2.add_into_branches(iter::once(IntoBranch::new(BACKPORT_BRANCH_NAME)));
    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge branch 'test-merge-backport' into test-merge",
    );
    utils::check_noop_merge(&origin_ctx, BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge branch 'test-merge-backport2' into test-merge-backport",
    );
    utils::check_noop_merge(&origin_ctx, BACKPORT_BRANCH_NAME);
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        &parent,
        "Merge topic 'topic-1' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME2,
        "Merge topic 'topic-1' into test-merge-backport2\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// backport2 -> backport -> branch (mr to just backports)
#[test]
fn test_merge_backport_into_backports_chain() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut merge_settings =
        MergeSettings::new(BACKPORT_BRANCH_NAME, utils::TestMergePolicy::default());
    merge_settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME2, utils::TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(BACKPORT_BRANCH_NAME);
    into_branch.chain_into(iter::once(IntoBranch::new(BRANCH_NAME)));
    merge_settings2.add_into_branches(iter::once(into_branch));

    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge branch 'test-merge-backport' into test-merge",
    );
    utils::check_noop_merge(&origin_ctx, BRANCH_NAME);
    let parent = format!("{}~", BRANCH_NAME);
    utils::check_mr_commit_message(&origin_ctx, &parent, "base: add a base commit\n");
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge branch 'test-merge-backport2' into test-merge-backport",
    );
    utils::check_noop_merge(&origin_ctx, BACKPORT_BRANCH_NAME);
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        &parent,
        "Merge topic 'topic-1' into test-merge-backport\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME2,
        "Merge topic 'topic-1' into test-merge-backport2\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// backport2 -> backport -> branch (mr to just ends)
#[test]
fn test_merge_backport_into_backports_ends() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
    let mut merge_settings2 =
        MergeSettings::new(BACKPORT_BRANCH_NAME2, utils::TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(BACKPORT_BRANCH_NAME);
    into_branch.chain_into(iter::once(IntoBranch::new(BRANCH_NAME)));
    merge_settings2.add_into_branches([into_branch, IntoBranch::new(BRANCH_NAME)]);

    let merge = utils::create_merge_many(&ctx, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let backports = [
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings, None),
    ];
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date(), backports)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge branch 'test-merge-backport' into test-merge",
    );
    utils::check_noop_merge(&origin_ctx, BRANCH_NAME);

    let parent = format!("{}~", BRANCH_NAME);
    utils::check_mr_commit_message(
        &origin_ctx,
        &parent,
        "Merge branch 'test-merge-backport2' into test-merge",
    );
    utils::check_noop_merge(&origin_ctx, &parent);
    let second_parent = format!("{}^2", BRANCH_NAME);
    utils::check_mr_commit_message(
        &origin_ctx,
        &second_parent,
        "Merge branch 'test-merge-backport2' into test-merge-backport",
    );
    utils::check_noop_merge(&origin_ctx, &second_parent);

    let grandparent = format!("{}~2", BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        &grandparent,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );

    let great_grandparent = format!("{}~3", BRANCH_NAME);
    utils::check_mr_commit_message(&origin_ctx, &great_grandparent, "base: add a base commit\n");

    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME,
        "Merge branch 'test-merge-backport2' into test-merge-backport",
    );
    utils::check_noop_merge(&origin_ctx, BACKPORT_BRANCH_NAME);
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    utils::check_mr_commit_message(&origin_ctx, &parent, "base: add a base commit\n");

    utils::check_mr_commit(
        &origin_ctx,
        BACKPORT_BRANCH_NAME2,
        "Merge topic 'topic-1' into test-merge-backport2\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    utils::check_mr_commit_message(&origin_ctx, &parent, "base: add a base commit\n");
}
